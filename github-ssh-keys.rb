#!/usr/bin/ruby

# Built with Ruby 2.2.4
# Uses `sshkey` by James Miller [https://github.com/bensie/sshkey]

# This script will allow a user to see the MD5 hashes of all public users
# belonging to a specified GitHub organization. The script will print `WEAK`
# next to any RSA keys of 1024 bits or below. At this time, only RSA keys are
# supported, DSA and ED25519 keys will be skipped over with an error message.
#
# This script will only work on organizations with 100 or less public members,
# as the organization API has an upper bound on returned users set to 100. API
# pagination will be a future feature, for now, track progress for this issue on
# the `unstable` branch.
#
# This has been developed for use with Ruby 2.2.4. Later versions should work
# fine, but they are untested.
# Make sure you have installed the `sshkey` gem with `gem install sshkey` or
# `bundle install` while inside this project.
#
# Usage: `ruby github-ssh-keys.rb rails`

require 'json'
require 'net/http'
require 'sshkey'

if ARGV.length != 1
  puts "Usage: 'ruby github-ssh-keys.rb [ORGANIZATION NAME]'"
  puts "Examples:"
  puts "         'ruby github-ssh-keys.rb rails'"
  puts "         'ruby github-ssh-keys.rb github'"
  exit 1
end

# Pull in the group name from the first argument
group_name = ARGV[0]
# We have to use `per_page=100` here because of GitHub's API pagination:
# https://developer.github.com/v3/#pagination
group_users_uri = URI("https://api.github.com/orgs/#{group_name}/members?per_page=100")
# Parse out the JSON so we can enumerate the users
group_users = JSON.parse Net::HTTP.get(group_users_uri)

# Print some nice-to-know information about the group
puts "Checking group #{group_name}"
puts "#{group_name} has #{group_users.length} members."

# For each user...
group_users.each do |user|
  # We'll set some variables, including the username, the URL where we can grab
  # their keys, and their ssh pubkeys.
  username = user["login"]
  user_key_uri = URI("https://github.com/#{username}.keys")
  user_keys = Net::HTTP.get(user_key_uri)
  # Here, we split out the keys on newline, one key per line.
  keys = user_keys.split("\n")

  # Now, we print...
  puts "\n=== #{username} ===\n\n"
  keys.each do |key|
    # Check for non-rsa keys, those aren't supported by the `sshkey` gem yet, so
    # we print a helpful debug message and move on.
    if key.split.first != "ssh-rsa"
      puts key.split.first + " keys are not currently supported. Skipping."
    else
      # For RSA keys, we set some variables for the key size and fingerprint
      key_bits = SSHKey.ssh_public_key_bits(key)
      key_fingerprint = SSHKey.md5_fingerprint(key)
      # then print it all out.
      # In addition, there's an inline if statement that will print `WEAK` if
      # the `key_bits` is less than 1024.
      puts "#{key_bits} MD5:#{key_fingerprint} (RSA)#{key_bits <= 1024 ? ' WEAK' : ''}"
    end
  end
end
