# GitHub SSH Keys

Uses `sshkey` by James Miller [https://github.com/bensie/sshkey]

This script will allow a user to see the MD5 hashes of all public users
belonging to a specified GitHub organization. The script will print `WEAK`
next to any RSA keys of 1024 bits or below. At this time, only RSA keys are
supported, DSA and ED25519 keys will be skipped over with an error message.

This script will only work on organizations with 100 or less public members, as
the organization API has an upper bound on returned users set to 100. API
pagination will be a future feature, for now, track progress for this issue on
the `unstable` branch.

This has been developed for use with Ruby 2.2.4. Later versions should work
fine, but they are untested.
Make sure you have installed the `sshkey` gem with `gem install sshkey` or
`bundle install` while inside this project.

## Usage

`ruby github-ssh-keys.rb rails`

`ruby github-ssh-keys.rb octopress`

## Notes

Built with Ruby 2.2.4

## Your task

Write a command-line script that will take the URL of a GitHub organization as
an argument. The script will go over all the publicly visible members in that
organization and will output a text report, listing the public SSH keys for each
person in the organization. The output must show the bit length and the MD5
fingerprint of each key. You don’t need to authenticate to fetch this data from
the GitHub API.

SSH keys shorter than 2048 bits will appear in the report with the tag WEAK
appended at the end of the entry.

Example usage:

`./github-ssh-keys https://github.com/rails`

Sample (fictional) output:

```
=== amatsuda ===

2048 MD5:09:54:d3:c3:20:6f:c0:e6:3e:05:a0:a6:21:7b:3c:7a (RSA)

=== arthurnn ===

4096 MD5:a4:ee:57:27:c2:51:ed:6b:40:86:c6:b3:d7:67:a5:7c (RSA)

1024 MD5:66:ad:80:f0:7a:0f:5a:34:ce:98:d2:77:84:e9:9d:4b (RSA) WEAK
```

You can write the script in Bash 3 (you can assume curl is installed), Ruby 2.2,
Python 2 or 3, or Perl 5. We’ll run the script on a Mac or an Ubuntu box. Let us
know if we need any special setup before running the script.
